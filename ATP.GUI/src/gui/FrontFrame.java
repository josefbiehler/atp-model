package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.JLabel;

import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JButton;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import apt.ParameterParser;
import apt.data.Import;
import apt.domain.Order;
import apt.domain.Period;
import apt.log.DummyLogger;
import apt.solve.SolOrders;
import apt.solve.SolverBase;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;

import javax.swing.Action;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Arrays;

import javax.swing.JCheckBox;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.BoxLayout;
import javax.swing.JTable;
import javax.swing.JScrollBar;

public class FrontFrame extends JFrame {

	private JPanel contentPane;
	private String orderPath;
	private String weightsPath;
	private boolean hasOrderNamedColumn;
	private boolean hasWeightsNamedColumns;
	private Import realImport;
	private JTable table;
	private JPanel solutionPanel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FrontFrame frame = new FrontFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FrontFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 652, 365);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane);
		
		JPanel dataImport = new JPanel();
		tabbedPane.addTab("Datenimport", null, dataImport, null);
		dataImport.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("127px"),
				ColumnSpec.decode("412px"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.LINE_GAP_ROWSPEC,
				RowSpec.decode("14px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		JLabel lblTest = new JLabel("Das korrekte Format entnehmen Sie bitte \r\nden Beispieldateien bzw. der Dokumentation");
		dataImport.add(lblTest, "2, 2, left, top");
		final JLabel lblOrderFile = new JLabel("");
		dataImport.add(lblOrderFile, "2, 4");
		
		final JCheckBox chckbxHasOrderNamed = new JCheckBox("benannt");
		chckbxHasOrderNamed.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				setHasOrderNamedColumn(chckbxHasOrderNamed.isSelected());
			}
		});
		dataImport.add(chckbxHasOrderNamed, "4, 4");
		final JLabel lblWeightsFile = new JLabel("");
		dataImport.add(lblWeightsFile, "2, 6");
		
		JButton btnOrderImport = new JButton("Auftragsimport");
		btnOrderImport.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				FileChooserReturn retVal = openFileChooser();
				if (retVal.file != null) {
					setOrderPath(retVal.file.getAbsolutePath());
					lblOrderFile.setText(retVal.file.getAbsolutePath());
				}
			}
		});
		dataImport.add(btnOrderImport, "1, 4");
		
		final JCheckBox chckbxHasWeightsNamed = new JCheckBox("benannt");
		chckbxHasWeightsNamed.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				setHasWeightsNamedColumns(chckbxHasWeightsNamed.isSelected());
			}
		});
		dataImport.add(chckbxHasWeightsNamed, "4, 6");
		
		JButton btnWeightsImport = new JButton("Gewichtungsimport");
		btnWeightsImport.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				FileChooserReturn retVal = openFileChooser();
				if (retVal.file != null) {
					setWeightsPath(retVal.file.getAbsolutePath());
					lblWeightsFile.setText(retVal.file.getAbsolutePath());
				}
			}
		});
		dataImport.add(btnWeightsImport, "1, 6");
		
		
		JButton btnStartImport = new JButton("Starte Import");
		
		
		JButton btnVerarbeite = new JButton("Verarbeite");
		btnVerarbeite.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				SolOrders solved = SolverBase.solve(getRealImport());
				fillTableWithContent(solved, contentPane);
			}
		});
		dataImport.add(btnVerarbeite, "2, 22");
		
		JPanel dataManual = new JPanel();
		tabbedPane.addTab("Manuelle Eingabe", null, dataManual, null);
		dataManual.setLayout(new BorderLayout(0, 0));
		
		JLabel lblDasKorrekteFormat = new JLabel("Das korrekte Format entnehmen Sie bitte den Beispieldateien bzw. der Dokumentation");
		dataManual.add(lblDasKorrekteFormat, BorderLayout.NORTH);
		
		dataImport.add(btnStartImport, "2, 8");
		
		JLabel lblStatistiken = new JLabel("Statistiken:");
		dataImport.add(lblStatistiken, "2, 10");
		
		JLabel lblImportierteBestellungen = new JLabel("Importierte Bestellungen");
		dataImport.add(lblImportierteBestellungen, "1, 12");
		
		final JLabel lblImportedOrders = new JLabel("");
		dataImport.add(lblImportedOrders, "2, 12");
		
		JLabel lblPerioden = new JLabel("Perioden:");
		dataImport.add(lblPerioden, "1, 14");
		
		final JLabel lblImportedPeriods = new JLabel("");
		dataImport.add(lblImportedPeriods, "2, 14");
		
		JLabel lblGewichte = new JLabel("Gewichte:");
		dataImport.add(lblGewichte, "1, 16");
		
		final JLabel lblImportedWeights = new JLabel("");
		dataImport.add(lblImportedWeights, "2, 16");
		
		JLabel lblAuftragszugnge = new JLabel("Auftragszug\u00E4nge:");
		dataImport.add(lblAuftragszugnge, "1, 18");
		
		final JLabel lblImportedInflow = new JLabel("");
		dataImport.add(lblImportedInflow, "2, 18");
		
		JLabel lblAnfangslagerbestand = new JLabel("Anfangslagerbestand:");
		dataImport.add(lblAnfangslagerbestand, "1, 20");
		
		final JLabel lblImportedL0 = new JLabel("");
		dataImport.add(lblImportedL0, "2, 20");
		
		JPanel solutionTab = new JPanel();
		tabbedPane.addTab("Visualisierung", null, solutionTab, null);
		solutionTab.setLayout(new BorderLayout(0, 0));
		
		solutionPanel = solutionTab;
		
		btnStartImport.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Import realImport = new Import(getOrderPath(), getWeightsPath(), new DummyLogger());
				
				if (realImport.startImport(isHasOrderNamedColumn(), isHasWeightsNamedColumns())) {
					lblImportedOrders.setText(String.valueOf(realImport.getVolume().length));
					lblImportedPeriods.setText(String.valueOf(realImport.getPeriods()));
					lblImportedWeights.setText(String.valueOf(realImport.getWeights().length));
					lblImportedInflow.setText(String.valueOf(realImport.getInventoryInflow().length));
					lblImportedL0.setText(String.valueOf(realImport.getInitialInventory()));
					setRealImport(realImport);
				}
				
			}
		});
	}
	
	private void fillTableWithContent(SolOrders solved, JPanel panel) {
		
		String[] column = new String[solved.getOrder().size()+1];
		Arrays.fill(column, "");
		DefaultTableModel model = new DefaultTableModel(column,0);
		
		JTable table = new JTable(model);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		String[] row = new String[solved.getOrder().size()+1];
		row[0] = "Auftrag";
		for(int i = 1; i < row.length; i++) {
			row[i] = "O" + i;
		}
		model.addRow(row);
		
		int[] bestPeriod = new int[solved.getOrder().size()];
		
		row[0] = "Wunschperiode";
		for(int i = 1; i < row.length; i++) {
			Order o = solved.getOrder().get(i-1);
			bestPeriod[i-1] = Period.getBestPeriodForOrder(o, solved.getPossiblePeriods());
			row[i] = String.valueOf(bestPeriod[i-1]);
		}
		
		model.addRow(row);
		
		row[0] = "Erfüllungsperiode";
		for(int i = 1; i < row.length; i++) {
			Order o = solved.getOrder().get(i-1);
			row[i] = String.valueOf(o.getPeriod().getId());
		}
		
		model.addRow(row);
		
		row[0] = "Verzögerung";
		for(int i = 1; i < row.length; i++) {
			Order o = solved.getOrder().get(i-1);
			row[i] = String.valueOf(o.getPeriod().getId()-bestPeriod[i-1]);
		}
		
		model.addRow(row);
		
		//String[][] rowData = {{"Auftrag", "O1", "O2"},{"Wunschtermin", "1", "2"},{"Erfüllungsperiode", "2", "1"},{"Verzögerung", "1", "-1"}};
		//table = new JTable();
		solutionPanel.add(table, BorderLayout.CENTER);
		
		solutionPanel.add(new JScrollPane(table));
		//JScrollBar scrollBar = new JScrollBar();
		//scrollBar.setOrientation(JScrollBar.HORIZONTAL);
		//panel.add(scrollBar, BorderLayout.SOUTH);
		
	}
	
	private FileChooserReturn openFileChooser() {
		JFileChooser chooser = new JFileChooser();
		int retVal = chooser.showOpenDialog(chooser);
		File file = null;
		
		if (retVal == JFileChooser.APPROVE_OPTION) {
			file = chooser.getSelectedFile();
		}
		
		return new FileChooserReturn(chooser, retVal, file);
	}

	public String getOrderPath() {
		return orderPath;
	}

	public void setOrderPath(String orderPath) {
		this.orderPath = orderPath;
	}

	public String getWeightsPath() {
		return weightsPath;
	}

	public void setWeightsPath(String weightsPath) {
		this.weightsPath = weightsPath;
	}

	public boolean isHasOrderNamedColumn() {
		return hasOrderNamedColumn;
	}

	public void setHasOrderNamedColumn(boolean hasOrderNamedColumn) {
		this.hasOrderNamedColumn = hasOrderNamedColumn;
	}

	public boolean isHasWeightsNamedColumns() {
		return hasWeightsNamedColumns;
	}

	public void setHasWeightsNamedColumns(boolean hasWeightsNamedColumns) {
		this.hasWeightsNamedColumns = hasWeightsNamedColumns;
	}

	public Import getRealImport() {
		return realImport;
	}

	public void setRealImport(Import realImport) {
		this.realImport = realImport;
	}
}

class FileChooserReturn {
	public JFileChooser chooser;
	public int retVal;
	public File file;
	
	public FileChooserReturn(JFileChooser chooser, int retVal, File file) {
		this.chooser = chooser;
		this.retVal = retVal;
		this.file = file;
	}
}
