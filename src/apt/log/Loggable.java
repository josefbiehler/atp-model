package apt.log;

public interface Loggable {
	void error(String msg, Exception e);
}
