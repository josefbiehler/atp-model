package apt;

import apt.log.Loggable;

public class ParameterParser {
	private Loggable log;
	
	public ParameterParser(Loggable log) {
		this.log = log;
	}
	public static String getOrderPath(String[] args) {
		// order path has index 0
		String path = args[0];
		return path;
	}
	
	public static String getWeightsPath(String[] args) {
		// weights path has index 1
		String path = args[1];
		return path;
	}
	
	/**
	 * default: true
	 * @param args
	 * @return
	 */
	public static boolean getOrderNamingColumns(String[] args) {
		boolean retVal = true;
		try {
			retVal = Boolean.parseBoolean(args[2]);
		}catch(Exception e) {
			//log.error("cannot parse " + args[2], e);
		}
		return retVal;
	}
	
	/**
	 * default: true
	 * @param args
	 * @return
	 */
	public static boolean getWeightsNamingColumns(String[] args) {
		boolean retVal = true;
		try {
			retVal = Boolean.parseBoolean(args[3]);
		}catch(Exception e) {
			//log.error("cannot parse " + args[3], e);
		}
		return retVal;
	}
}
