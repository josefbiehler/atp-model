package apt;

import java.util.ArrayList;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.config.solver.XmlSolverFactory;

import apt.data.GenerateData;
import apt.domain.Period;
import apt.solve.SolOrders;
import apt.solve.SolverBase;

public class MainClass {

	public static void main(String[] args) {
		// call from command line with arguments: orderpath weightpath ordernamed weightnamed
		if (args.length > 0) {
			System.out.println(SolverBase.importAndSolve("/apt/solve/config.xml", ParameterParser.getOrderPath(args), 
					ParameterParser.getWeightsPath(args), ParameterParser.getOrderNamingColumns(args), ParameterParser.getWeightsNamingColumns(args)));
		}
	}

}
