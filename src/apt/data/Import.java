package apt.data;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import apt.log.Loggable;
import au.com.bytecode.opencsv.CSVReader;

public class Import {
	private String pathToOrder;
	private String pathToWeights;
	private int[] volume;
	private int initialInventory;
	private int[] inventoryInflow;
	private int periods;
	private int[] weights;
	public Loggable log;
	
	public Import(String pathToOrder, String pathToWeights, Loggable logger) {
		this.pathToOrder = pathToOrder;
		this.pathToWeights = pathToWeights;
		log = logger;
	}

	/**
	 * take a look into the "apt/data/examples" directory to get into the format
	 * @param reader
	 * @param hasNamingColumn
	 * @throws IOException 
	 * @throws Exception
	 */
	public void importOrders(CSVReader reader, boolean hasNamingColumn) throws IOException {
	    String [] csvVolume = reader.readNext();
	    String [] csvInitialInventory = reader.readNext();
	    String [] csvInventoryInflow = reader.readNext();
	    String [] csvPeriods = reader.readNext();
	    
	    int subForNamingColumn = 0;
	    
	    if (hasNamingColumn) {
	    	subForNamingColumn = 1;
	    }
	    
	    volume = new int[csvVolume.length-subForNamingColumn];
	    initialInventory = Integer.parseInt(csvInitialInventory[subForNamingColumn]);
	    inventoryInflow = new int[csvInventoryInflow.length-subForNamingColumn];
	    periods = Integer.parseInt(csvPeriods[subForNamingColumn]);
	    
	    for(int i = 0; i < csvVolume.length-subForNamingColumn; i++) {
	    	volume[i] = Integer.parseInt(csvVolume[i+subForNamingColumn]);
	    }
	    
	    for(int i = 0; i < csvInventoryInflow.length-subForNamingColumn; i++) {
	    	inventoryInflow[i] = Integer.parseInt(csvInventoryInflow[i+subForNamingColumn]);
	    }
	}
	
	/**
	 * take a look into the "apt/data/examples" directory to get into the format
	 * @param reader
	 * @param hasNamingColumns
	 * @return
	 * @throws Exception
	 */
	public List<Integer[]> importWeights(CSVReader reader, boolean hasNamingColumns, int periods) throws IOException {
		int subForNamingColumns = 0;
		
		if (hasNamingColumns) {
			reader.readNext();
			subForNamingColumns = 1;
		}
		
	    List<Integer[]> listOfPeriods = new ArrayList<Integer[]>();
	    
	    for(int p = 0; p < periods; p++) {
	    	String[] line = reader.readNext();
	    	Integer[] period_p = new Integer[periods];
	    	listOfPeriods.add(period_p);
	    	
	    	for(int vol = subForNamingColumns; vol < line.length; vol++) {
	    		int weight;
	    		try {
	    			weight = Integer.parseInt(line[vol]);
	    		}catch(NumberFormatException e) {
	    			weight = 0;
	    		}
	    		period_p[vol-subForNamingColumns]=weight;
	    	}
	    }
	    
	    return listOfPeriods;
	}
	
	/**
	 * Take arrays of weights for (order|period) like : Order1_weight -> [1,0,1,0], Order2_weight -> [0,1,1,1] and combine them like:
	 * 	[1,0,1,0,0,1,1,1]
	 * @param listOfPeriods a list of Integer arrays. Every array holds the weights for one period and all orders.
	 * 		If we use the example above, the list will look like following:
	 * 			[
	 * 				period1: [1,0]
	 * 				period2: [0,1]
	 * 				period3: [1,1]
	 * 				period4: [0,1]
	 * 			]
	 */
	public void mergeAllWeights(List<Integer[]> listOfPeriods) {
	    weights = new int[listOfPeriods.size()*(listOfPeriods.get(0)).length];
	    int weightsIndex = 0;
	    
	    for(int o = 0; o < volume.length; o++) {
	    	for(int p = 0; p < periods; p++) {
	    		weights[weightsIndex] = listOfPeriods.get(p)[o];
	    		weightsIndex++;
	    	}
	    }
	}
	
	public boolean startImport(boolean hasOrderNamingColumn, boolean hasWeightNamingColumns) {
		CSVReader reader = null;
		
		try {
	    reader = new CSVReader(new FileReader(this.pathToOrder),';');
			try {
			    importOrders(reader, hasOrderNamingColumn);
			    try {
				    reader = new CSVReader(new FileReader(this.pathToWeights),';');
				    mergeAllWeights(importWeights(reader, hasWeightNamingColumns, getPeriods()));
				    return true;
			    }catch(IOException e) {
			    	log.error("error while reading from weights file", e);
			    }
			} catch(IOException e) {
				log.error("error while trying to read from orders file", e);
			}
		} catch(FileNotFoundException e) {
			log.error("cannot find file: " + this.pathToOrder, e);
		} finally {
			try {
				reader.close();
			} catch (Exception e) {
				log.error("error while closing CSVReader", e);
			}
		}
		return false;
	}

	public int[] getVolume() {
		return volume;
	}

	public void setVolume(int[] volume) {
		this.volume = volume;
	}

	public int getInitialInventory() {
		return initialInventory;
	}

	public void setInitialInventory(int initialInventory) {
		this.initialInventory = initialInventory;
	}

	public int[] getInventoryInflow() {
		return inventoryInflow;
	}

	public void setInventoryInflow(int[] inventoryInflow) {
		this.inventoryInflow = inventoryInflow;
	}

	public int getPeriods() {
		return periods;
	}

	public void setPeriods(int periods) {
		this.periods = periods;
	}

	public int[] getWeights() {
		return weights;
	}

	public void setWeights(int[] weights) {
		this.weights = weights;
	}
}
