package apt.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import apt.domain.Order;
import apt.domain.Period;

public class GenerateData {
	/**
	 * Create objects out of integer arrays
	 * @param periods
	 * @param orderWeights
	 * @return
	 */
	public static ArrayList<Period> generatePeriods(int periods, Map<Order, int[]> orderWeights) {
		ArrayList<Period> retVal = new ArrayList<Period>();
		for(int p = 1; p <= periods; p++) {
			Period period = new Period(p);
			for(Entry<Order, int[]> entry : orderWeights.entrySet()) {
				period.addPeriodOrder(entry.getKey(),  entry.getValue()[p-1]);
			}
			retVal.add(period);
		}
		return retVal;
	}
	
	/**
	 * generate objects out of integer arrays
	 * @param orderVolumes
	 * @param orderPeriodWeights
	 * @param periods
	 * @return
	 */
	public static Map<Order, int[]> generateOrderWeights(int[] orderVolumes, int[] orderPeriodWeights, int periods) {
		Map<Order, int[]> retVal = new HashMap<Order, int[]>();
		for(int i = 1; i <= orderVolumes.length; i++) {
			Order order = new Order(i, orderVolumes[i-1]);
			int[] weights = Arrays.copyOfRange(orderPeriodWeights, periods*(i-1), periods*(i));
			retVal.put(order,  weights);
		}
		return retVal;
	}
	
}
