
package apt.domain;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@PlanningEntity
/*
 * The Order is the planning entity.
 * That involves that a order cannot be linked with two different periods.
 * So the corresponding constraint F.3 (referring to Tempelmeier) never gets violated.
 */
public class Order extends Domain{
	private int volume;
	private int num;
	private Period period;
	
	public Order() {}
	
	public Order(int num, int volume) {
		this.num = num;
		this.volume = volume;
		setId(num);
	}
	
	@PlanningVariable(valueRangeProviderRefs = {"periods"})
	/*
	 * A period is the part that changes during solving the problem.
	 */
	public Period getPeriod() {
		return this.period;
	}
	
	public void setPeriod(Period p) {
		this.period = p;
	}
	
	public int getVolume() {
		return this.volume;
	}
}
