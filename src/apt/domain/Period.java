package apt.domain;

import java.util.ArrayList;
import java.util.List;

/*
 * A period is the part that will be assigned to an order during solving the problem.
 */
public class Period extends Domain{
	private int num;
	private ArrayList<PeriodOrder> periodOrders;

	public Period() {}
	
	public Period(int num) {
		this.num = num;
		periodOrders = new ArrayList<PeriodOrder>();
		setId(num);
	}
	
	public void addPeriodOrder(Order order, int weight) {
		PeriodOrder po = new PeriodOrder(order, this, weight);
		periodOrders.add(po);
	}
	
	public int getWeightForOrder(Order o) {
		for(PeriodOrder po : periodOrders) {
			if (po.getOrder().equals(o)) {
				return po.getWeight();
			}
		}
		// 0 is used as weight if no rules are present for that combination of (order|period).
		// if we use 1 then the result will be influenced by not relevant data
		return 0;
	}
	
	public static int getBestPeriodForOrder(Order o, List<Period> periods) {
		int highestWeight = 0;
		int period = 0;
		for(Period p : periods) {
			int weight = p.getWeightForOrder(o);
			if (weight > highestWeight) {
				highestWeight = weight;
				period = p.getId();
			}
		}
		return period;
	}
}
