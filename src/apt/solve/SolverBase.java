package apt.solve;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.config.solver.XmlSolverFactory;

import apt.data.Import;

public class SolverBase {
	public static SolOrders importAndSolve(String configPath, String order, String weight, boolean orderNamed, boolean weightNamed) {
		SolverFactory solverFact = new XmlSolverFactory(configPath);
		Solver solver = solverFact.buildSolver();
		SolOrders unsolved = new SolOrders(order, weight, orderNamed, weightNamed);
		solver.setPlanningProblem(unsolved);
		solver.solve();
		SolOrders solved = (SolOrders)solver.getBestSolution();
		return solved;
	}
	
	public static SolOrders solve(Import realImport) {
		SolverFactory solverFact = new XmlSolverFactory("/apt/solve/config.xml");
		Solver solver = solverFact.buildSolver();
		SolOrders unsolved = new SolOrders(realImport);
		solver.setPlanningProblem(unsolved);
		solver.solve();
		SolOrders solved = (SolOrders)solver.getBestSolution();
		return solved;
	}
}
