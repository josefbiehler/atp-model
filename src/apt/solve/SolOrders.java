package apt.solve;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.value.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.simple.SimpleScore;
import org.optaplanner.core.impl.solution.Solution;

import apt.data.GenerateData;
import apt.data.Import;
import apt.domain.Order;
import apt.domain.Period;
import apt.log.DummyLogger;

@PlanningSolution
public class SolOrders implements Solution<SimpleScore> {

	private List<Order> orders;
	private List<Period> periods;
	private SimpleScore score;
	public int[] lagerzugang;
	public int[] lagerbestand;
	
	public SolOrders() {}
	
	public SolOrders(Import i) {
		int[] os = i.getVolume();
		int ps = i.getPeriods();
		int[] weights = i.getWeights();
		int[] q1 = i.getInventoryInflow();
		int[] q2 = new int[ps+1];
		Arrays.fill(q2, 0);
		q2[0] = i.getInitialInventory();
		
		lagerzugang = q1;
		lagerbestand = q2;
		
		Map<Order, int[]> orderWeights = GenerateData.generateOrderWeights(os, weights, ps);
		ArrayList<Period> p = GenerateData.generatePeriods(ps, orderWeights);
		periods = p;
		orders = new ArrayList<Order>();
		for(Entry<Order,int[]> entry : orderWeights.entrySet()) {
			orders.add(entry.getKey());
		}
	}
	
	public SolOrders(String pathToOrder, String pathToWeights, boolean hasOrderNamedColumn, boolean hasWeightNamedColumns) {
		Import i = new Import(pathToOrder, pathToWeights, new DummyLogger());
		try {
			i.startImport(hasOrderNamedColumn, hasWeightNamedColumns);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		int[] os = i.getVolume();
		int ps = i.getPeriods();
		int[] weights = i.getWeights();
		int[] q1 = i.getInventoryInflow();
		int[] q2 = new int[ps+1];
		Arrays.fill(q2, 0);
		q2[0] = i.getInitialInventory();
		
		lagerzugang = q1;
		lagerbestand = q2;
		
		Map<Order, int[]> orderWeights = GenerateData.generateOrderWeights(os, weights, ps);
		ArrayList<Period> p = GenerateData.generatePeriods(ps, orderWeights);
		periods = p;
		orders = new ArrayList<Order>();
		for(Entry<Order,int[]> entry : orderWeights.entrySet()) {
			orders.add(entry.getKey());
		}
		
	}
	@ValueRangeProvider(id="periods") 
	public List<Period> getPossiblePeriods() {
		return periods;
	}
	
	@PlanningEntityCollectionProperty
	public List<Order> getOrder() {
		return orders;
	}
	
	@Override
	public Collection<? extends Object> getProblemFacts() {
		return orders;
	}

	@Override
	public SimpleScore getScore() {
		return score;
	}

	@Override
	public void setScore(SimpleScore arg0) {
		score = arg0;
	}
	
	@Override
	public String toString() {
		String s = "";
		for(Order o : orders) {
			s = s + "nr: " + o.getId() + "Periode: "  + o.getPeriod().getId() + "\r\n";
		}
		return s;
	}

}
