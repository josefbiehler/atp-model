package apt.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import apt.ParameterParser;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

@RunWith(JUnit4.class)
public class ParameterParserTest {

	private String[] args;
	
	@Before
	public void setUp() {
		args = new String[4];
		args[0] = "test1";
		args[1] = "test2";
		args[2] = "false";
		args[3] = "true";
	}
	
	@Test
	public void testGetOrderNamingColumns() {
		assertThat(ParameterParser.getOrderNamingColumns(args), is(false));
		args[2] = "true";
		assertThat(ParameterParser.getOrderNamingColumns(args), is(true));
	}
	
	@Test
	public void testGetWeightsNamingColumns() {
		assertThat(ParameterParser.getWeightsNamingColumns(args), is(true));
		args[3] = "false";
		assertThat(ParameterParser.getWeightsNamingColumns(args), is(false));
	}
	
	@Test
	public void testGetOrderPath() {
		assertThat(ParameterParser.getOrderPath(args), is("test1"));
	}
	
	@Test
	public void testGetWeightsPath() {
		assertThat(ParameterParser.getWeightsPath(args), is("test2"));
	}
}
