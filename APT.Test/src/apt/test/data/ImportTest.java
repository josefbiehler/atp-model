package apt.test.data;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import apt.data.Import;
import apt.log.Loggable;
import au.com.bytecode.opencsv.CSVReader;
import static org.junit.Assert.*;

import org.hamcrest.CoreMatchers;

@RunWith(JUnit4.class)
public class ImportTest {
	private Loggable logger;
	private Import realImport;
	
	@Before
	public void setUp() {
		logger = Mockito.mock(Loggable.class);
	}
	
	@Test
	public void testStartImportCatchNotFound() {
		realImport = new Import("test.csv", "", logger);
		realImport.startImport(true, true);
		Mockito.verify(logger, Mockito.times(1)).error(Mockito.eq("cannot find file: test.csv"), Mockito.any(Exception.class));
	}
	
	@Test
	public void testStartImportCanOpenFiles() throws IOException {
		realImport =  new Import("bin/apt/test/data/examples/order.csv", "bin/apt/test/data/examples/gewichtung.csv", logger);
		Import partialMockedImport = Mockito.spy(realImport);
		Mockito.doNothing().when(partialMockedImport).importOrders(Mockito.any(CSVReader.class), Mockito.any(Boolean.class));
		Mockito.doReturn(null).when(partialMockedImport).importWeights(Mockito.any(CSVReader.class), Mockito.any(Boolean.class), Mockito.any(Integer.class));
		Mockito.doNothing().when(partialMockedImport).mergeAllWeights(Mockito.anyListOf(Integer[].class));
		assertThat(partialMockedImport.startImport(true, true), CoreMatchers.is(true));
	}

	@Test
	public void testImportOrderWithNamingColumn() throws IOException {
		CSVReader reader = new CSVReader(new FileReader("bin/apt/test/data/examples/order.csv"), ';');
		realImport = new Import("", "", logger);
		realImport.importOrders(reader, true);
		assertThat(realImport.getInitialInventory(), CoreMatchers.is(20));
		assertThat(realImport.getPeriods(), CoreMatchers.is(5));
		assertThat(Arrays.asList(realImport.getInventoryInflow()), CoreMatchers.hasItems(new int[]{20,0,100,0,0}));
		assertThat(Arrays.asList(realImport.getVolume()), CoreMatchers.hasItems(new int[]{10,30,40,30,20}));
	}
	
	@Test
	public void testImportWeightsWithNamingColumns() throws IOException {
		CSVReader reader = new CSVReader(new FileReader("bin/apt/test/data/examples/gewichtung.csv"), ';');
		realImport = new Import("", "", logger);
		List<Integer[]> list = realImport.importWeights(reader, true, 5);
		assertThat(list.size(), CoreMatchers.is(5));
		assertThat(Arrays.asList(list.get(0)), CoreMatchers.hasItems(new Integer[]{0,0,3,0,5}));
		assertThat(Arrays.asList(list.get(1)), CoreMatchers.hasItems(new Integer[]{0,0,0,0,0}));
		assertThat(Arrays.asList(list.get(2)), CoreMatchers.hasItems(new Integer[]{-1,0,0,0,0}));
		assertThat(Arrays.asList(list.get(3)), CoreMatchers.hasItems(new Integer[]{-2,0,0,0,0}));
		assertThat(Arrays.asList(list.get(4)), CoreMatchers.hasItems(new Integer[]{-2,0,0,0,0}));
		
	}
}
