package apt.test.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import apt.data.GenerateData;
import apt.domain.Order;
import apt.domain.Period;

@RunWith(JUnit4.class)
public class GenerateDataTest {
	@Test
	public void testGenerateOrderWeights() {
		int[] volume = {10, 20, 30, 40};
		int periods = 5;
		int[] orderPeriodWeights = {1, 0, 0, 0, 0, 2, 3, 0, 0, 0, 5, 5, 5, 5, 5, 0, 0, 0, 4, 0};
		Map<Order, int[]> orderWeights = GenerateData.generateOrderWeights(volume, orderPeriodWeights, periods);
		
		// run through map, check correct assignment of weights
		for(Entry<Order, int[]> entry : orderWeights.entrySet()) {
			Order order = entry.getKey();
			int[] weights = entry.getValue();
			int id = order.getId()-1; // id and num are equal, but both begins with 1, so minus 1
			// calculate start and end of weights for order with id "id" within orderPerodWeights
			int startIndex = id*periods;
			// endIndex is excluded
			int endIndex = id*periods+periods;
			for(int i = startIndex, q = 0; i < endIndex; i++, q++) {
				assertThat(weights[q], is(orderPeriodWeights[i]));
			}
		}
	}
	
	@Test
	public void testGeneratePeriods() {
		Map<Order, int[]> orderWeights = new HashMap<>();
		Order o1 = new Order(1, 123);
		Order o2 = new Order(2, 234);
		int periods = 3;
		int[] wO1 = new int[]{1,2,3};
		int[] wO2 = new int[]{3,0,0};
		
		orderWeights.put(o1, wO1);
		orderWeights.put(o2, wO2);
		List<Period> periodList = GenerateData.generatePeriods(periods, orderWeights);
		
		Period p1 = periodList.get(0);
		Period p2 = periodList.get(1);
		Period p3 = periodList.get(2);
		
		assertThat(p1.getWeightForOrder(o1), is(1));
		assertThat(p2.getWeightForOrder(o1), is(2));
		assertThat(p3.getWeightForOrder(o1), is(3));
		assertThat(p1.getWeightForOrder(o2), is(3));
		assertThat(p2.getWeightForOrder(o2), is(0));
		assertThat(p3.getWeightForOrder(o2), is(0));
		
		
	}
}
